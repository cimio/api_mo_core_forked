package com.cinepolis.app.apimocore.repository;

import com.cinepolis.app.apimocore.model.User;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface UserRepository extends MongoRepository<User, String> {
    User findByEmployeeNumber(String employeeNumber);
}
