package com.cinepolis.app.apimocore.model;

import lombok.Data;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.index.IndexDirection;
import org.springframework.data.mongodb.core.index.Indexed;
import org.springframework.data.mongodb.core.mapping.DBRef;
import org.springframework.data.mongodb.core.mapping.Document;

import java.util.List;

@Data
@Document(collection = "users")
public class User {
    @Id
    private String id;
    private String name;
    private String lastName;
    private Long cinemaId;
    @Indexed(unique = true, direction = IndexDirection.DESCENDING)
    private String employeeNumber;
    private String password;
    private Boolean isEnabled;
    @DBRef
    private List<Role> roles;
}
