package com.cinepolis.app.apimocore.service;

import com.cinepolis.app.apimocore.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;

@Service
public class UserService implements UserDetailsService {

    private UserRepository userRepository;

    @Autowired
    public UserService(UserRepository userRepository) {
        this.userRepository = userRepository;
    }

    /**
     * In this method <code>username</code> is used as employee number
     *
     * @param username The employee number
     * @return User from userdetails {@link org.springframework.security.core.userdetails}
     * @throws UsernameNotFoundException The exception
     * @Author amorenoc@cinepolis.com
     */
    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
        com.cinepolis.app.apimocore.model.User user = userRepository.findByEmployeeNumber(username);
        List<GrantedAuthority> authorities = user.getRoles()
                .stream()
                .map(role -> new SimpleGrantedAuthority(role.getName()))
                .collect(Collectors.toList());

        return new User(user.getEmployeeNumber(), user.getPassword(), true, true, true, true, authorities);
    }
}
